var express = require('express');
var router = express.Router();
var NodeGeocoder = require('node-geocoder');
var ig = require('instagram-node').instagram();


//  !start geocoding
var options = {
  provider: 'google',
  httpAdapter: 'https',
  apiKey: 'AIzaSyDpSxBLHekTdwZjRMEEjHYenwNP6_znvlM',
  formatter: null
};

var geocoder = NodeGeocoder(options);
//  !end geocoding

//  !start Instagram
const accessToken_Inst = '6259607829.e029fea.963bc62c8f5e45e78f869eacae92fbe3';

ig.use({ access_token: accessToken_Inst });
//  !end Instagram

//  Use geocoder for search latitude and longitude of a location
function searchMap(request, callback) {

  geocoder.geocode(request, function(err, res) {

    if (err) {
      callback(err, null);
    }
    callback(false, res);
  });
};

//  Router index page
router.get('/', function(req, res, next) {
  res.render('index', {title: 'Search'});
});

//  Search post method
router.post('/', function(req, res, next) {

  var request = req.body.q;
  console.log(request);

  searchMap(request, function(err, data) {

    if (err) {
      return res.json(404);
    } else if (typeof data[0] === 'undefined') {
      return res.render('index', {title: 'Search: lỗi', err: true});
    }

    const lat = data[0].latitude;
    const lng = data[0].longitude;
    const option = {distance: 400};

    ig.location_search({ lat: lat, lng: lng }, 
                         option,
                         function(err, result, remaining, limit) {

      if (err) {
        return res.json(404);
      } 
      return res.render('index', {title: 'Search: ' + request, items: result, err: false});
    });
  });
});

//  Display all recent image get from location_id
router.get('/location/:id', function(req, res, next) {

  var id = req.params.id;
  const option = {};

  ig.location_media_recent(id, option, function(err, result, pagination, remaining, limit) {
    if (err) {
      return res.json(404);
    }

    var items = [];
    for (var i = 0; i < result.length; i++) {
      items[i] = result[i].images.thumbnail.url;
    }

    return res.render('location', {title: 'Location', items: items});
  });
});


module.exports = router;
